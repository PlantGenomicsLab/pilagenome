#!/bin/bash
#SBATCH --job-name=agat_filter_gff
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

source activate agat 

agat_sp_filter_feature_from_keep_list.pl --gff /core/labs/Wegrzyn/ConiferGenomes/pila_2.0_june_2023/assessments/invest_stringtie/transdecoder/lowresource_stringtie.transdecoder.genome.gff3 --keep_list transcripts_with_eggmatch.txt --output redo2_stringtie.transdecoder.genome.eggnogfilt.gff3

comm -3 transcripts_with_eggmatch.txt agat_kept_records.txt > extra_agat_trans.txt

agat_sp_filter_feature_from_kill_list.pl --gff redo2_stringtie.transdecoder.genome.eggnogfilt.gff3 --kill_list extra_agat_trans.txt --output test_kill_list.gff3 



echo -e "\nEnd time:"
date

