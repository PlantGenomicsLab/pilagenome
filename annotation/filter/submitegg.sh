#!/bin/bash
#SBATCH --job-name=submitegg
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 8
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=40G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

source activate eggnog

emapper.py -i /core/labs/Wegrzyn/ConiferGenomes/pila_2.0_june_2023/assessments/invest_stringtie/transdecoder/remapped.pep.fa -o eggnog.blastp -m diamond --cpu 8




echo -e "\nEnd time:"
date

