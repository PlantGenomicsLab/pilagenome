#!/bin/bash
#SBATCH --job-name=filter_proteins
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=5G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date


module load seqkit/2.2.0

#seqkit grep -f transcripts_with_eggmatch.txt /core/labs/Wegrzyn/ConiferGenomes/pila_2.0_june_2023/assessments/invest_stringtie/transdecoder/remapped.pep.fa > post_eggnog_filter.pep.fa
seqkit grep -f transcripts_with_eggmatch.txt /core/labs/Wegrzyn/ConiferGenomes/pila_2.0_june_2023/assessments/invest_stringtie/transdecoder/remapped.cds.fa > post_eggnog_filter.cds.fa 

#seqkit grep -f transcripts_with_eggmatch.txt transdecoder/retrygffread_transcripts.fa.transdecoder.cds > post_interproscan_filter.cds.fa


echo -e "\nEnd time:"
date

