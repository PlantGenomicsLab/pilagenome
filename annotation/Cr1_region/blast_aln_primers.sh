#!/bin/bash
#SBATCH --job-name=aln
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

module load blast/2.11.0
#blastn -task blastn -num_threads 4 -db /core/labs/Wegrzyn/ConiferGenomes/pila_2.0_june_2023/cr1_related/old_blast/sp_split_chr -query ../seq.fa -out primer.out.standard
#blastn -task blastn -num_threads 4 -db /core/labs/Wegrzyn/ConiferGenomes/pila_2.0_june_2023/cr1_related/old_blast/sp_split_chr -query ../seq2.fa -out primer2.out.standard
#blastn -num_threads 4 -db /core/labs/Wegrzyn/ConiferGenomes/pila_2.0_june_2023/cr1_related/old_blast/sp_split_chr -query ../seq3.fa -out seq5701.out.standard
#blastn -num_threads 4 -db /core/labs/Wegrzyn/ConiferGenomes/pila_2.0_june_2023/cr1_related/old_blast/sp_split_chr -query ../seq3_dash.fa -out seq5701_dash.out.standard

#blastn -task blastn -num_threads 4 -db /core/labs/Wegrzyn/ConiferGenomes/pila_2.0_june_2023/cr1_related/old_blast/sp_split_chr -query ../seq.fa -out primer.outfmt -outfmt "6 qseqid sseqid length pident evalue bitscore qstart qend sstart send"

#blastn -task blastn -num_threads 4 -db /core/labs/Wegrzyn/ConiferGenomes/pila_2.0_june_2023/cr1_related/old_blast/sp_split_chr -query ../seq2.fa -outfmt "6" -out primer2.outfmt.txt

blastn -num_threads 4 -db /core/labs/Wegrzyn/ConiferGenomes/pila_2.0_june_2023/cr1_related/old_blast/sp_split_chr -query monticola_putative_tnl.fa -outfmt "6" -out monticola_putative_tnl.outfmt 


echo -e "\nEnd time:"
date

