#!/bin/bash
#SBATCH --job-name=flag_dup
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

module load picard/2.23.9

java -jar /isg/shared/apps/picard/2.23.9/picard.jar MarkDuplicates \
      I=primary_filt_selected_sorted_SRR3689473.bam \
      O=flagdup_primary_filt_selected_sorted_SRR3689473.bam \
      M=marked_dup_metrics.txt



echo -e "\nEnd time:"
date

