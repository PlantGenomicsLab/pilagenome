#!/bin/bash
#SBATCH --job-name=align
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 20
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=150G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date
module load hisat2/2.2.1
module load samtools 

for f in /core/projects/EBP/Wegrzyn/pila/rnaseq/*_1.fastq.gz
do 
i=$(echo $f | sed 's/_1.fastq.gz//')
b=$(basename $i)
hisat2 -x top12_split --max-intronlen 2500000 -1 "${i}_1.fastq.gz" -2 "${i}_2.fastq.gz" -p 20 -S "${b}.sam" --dta
samtools view -@ 20 -uhS "${b}.sam" | samtools sort -@ 20 -o "sorted_${b}.bam"
#rm "${b}.sam"
done 


echo -e "\nEnd time:"
date

