#!/bin/bash
#SBATCH --job-name=fastp_SRR3723921
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=8G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date

module load fastqc/0.11.7

~/fastp -i SRR3723921_1.fastq -I SRR3723921_2.fastq -o fastp_SRR3723921_1.fastq -O fastp_SRR3723921_2.fastq -l 50 --thread=4

date
