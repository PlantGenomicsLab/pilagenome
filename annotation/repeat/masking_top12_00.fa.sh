#!/bin/bash
#SBATCH --job-name=repeatmask_top12_00.fa
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 20
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=60G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date

module load perl/5.28.1
export PATH=/core/labs/Wegrzyn/annotationtool/software/RepeatMasker/4.0.6:$PATH

RepeatMasker -pa 20 -gff -a -noisy -low -xsmall -lib /core/labs/Wegrzyn/ConiferGenomes/pila_2.0_june_2023/repeats/top12/sugarpine2_top12-families.fa top12_00.fa
date
