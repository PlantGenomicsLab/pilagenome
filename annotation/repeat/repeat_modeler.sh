#!/bin/bash
#SBATCH --job-name=model
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=200G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date


module unload perl/5.28.0
module load perl/5.24.0
export PERL5LIB=/UCHC/PublicShare/szaman/perl5/lib/perl5/
#module load RepeatModeler/2.01
module load RepeatModeler/2.0.4
module load genometools/1.6.1
module load mafft/7.471
export LTRRETRIEVER_PATH=/core/labs/Wegrzyn/annotationtool/software/LTR_retriever
module load cdhit/4.8.1
module load ninja/0.95
export TMPDIR=/core/labs/Wegrzyn/whitebarkpine/

BuildDatabase -name sugarpine2_top12 /core/labs/Wegrzyn/ConiferGenomes/pila_2.0_june_2023/top_chromosomes/split_chr_top_12.fa

RepeatModeler -database sugarpine2_top12 -threads 16

#module load blast
#makeblastdb -dbtype nucl -out sugarpine2.2 -in /core/labs/Wegrzyn/ConiferGenomes/pila_2.0_june_2023/sp2.0.nochlor.renamed.fa




echo -e "\nEnd time:"
date