#!/bin/bash
#SBATCH --job-name=hm2genome
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 32
#SBATCH --partition=himem2
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=1000G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

module load busco/5.4.5
module load MetaEuk/4.0

busco -f -c 36 -i /core/labs/Wegrzyn/ConiferGenomes/pila_2.0_june_2023/repeats/maskrepeats/combined_top12_masked_sugarpine.fa -l embryophyta_odb10 -o busco_genometop_himem -m genome 

echo -e "\nEnd time:"
date

