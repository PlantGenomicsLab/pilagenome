#!/bin/bash
#SBATCH --job-name=busco_eggnog
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=30G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date


module load busco/5.4.5

busco -c 12 -i /core/labs/Wegrzyn/ConiferGenomes/pila_2.0_june_2023/assessments/invest_stringtie/eggnog/post_eggnog_filter.pep.fa -l embryophyta_odb10 -o busco_eggnog_filt -m protein 



echo -e "\nEnd time:"
date

