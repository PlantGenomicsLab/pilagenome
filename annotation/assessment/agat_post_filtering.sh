#!/bin/bash
#SBATCH --job-name=after_egg
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

source activate agat 

#agat_sp_statistics.pl --gff /core/labs/Wegrzyn/ConiferGenomes/pila_2.0_june_2023/assessments/invest_stringtie/eggnog/pila2_filtered_annotation.gff -o after_egg_filt_stats.txt

#agat_sp_filter_incomplete_gene_coding_models.pl --gff /core/labs/Wegrzyn/ConiferGenomes/pila_2.0_june_2023/assessments/invest_stringtie/eggnog/pila2_filtered_annotation.gff --fasta /core/labs/Wegrzyn/ConiferGenomes/pila_2.0_june_2023/sugar_pine_genome_split_chr.fa -o filter_incomplete_pila2_filtered_annotation.gff 


#agat_sp_add_start_and_stop.pl --gff /core/labs/Wegrzyn/ConiferGenomes/pila_2.0_june_2023/assessments/invest_stringtie/eggnog/pila2_filtered_annotation.gff --fasta /core/labs/Wegrzyn/ConiferGenomes/pila_2.0_june_2023/sugar_pine_genome_split_chr.fa -o add_start_stop_pila2_annnotation.gff 




#agat_sp_statistics.pl --gff filter_incomplete_pila2_filtered_annotation.gff  -o filter_incomplete_pila2_annotation.stats
#agat_sp_statistics.pl --gff add_start_stop_pila2_annnotation.gff -o add_start_stop_pila2_annotation.stats


#agat_sp_filter_incomplete_gene_coding_models.pl --gff add_start_stop_pila2_annnotation.gff --fasta /core/labs/Wegrzyn/ConiferGenomes/pila_2.0_june_2023/sugar_pine_genome_split_chr.fa -o filter_incomplete_after_add_startstop.gff 

agat_sp_statistics.pl --gff filter_incomplete_after_add_startstop.gff -o filter_incomplete_after_add_startstop.stats



echo -e "\nEnd time:"
date

