#!/bin/bash
#SBATCH --job-name=stringtie_SRR3723924
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=40G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname 
date

module load stringtie/2.2.1
stringtie -o SRR3723924_stringtie.gtf -l PILA -p 8 /core/labs/Wegrzyn/ConiferGenomes/pila_2.0_june_2023/split_chromosome_hisat/top12/resorted_SRR3723924.bam

date
