#!/bin/bash
#SBATCH --job-name=st_remerge
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=60G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

module load stringtie/2.2.1
stringtie --merge -p 8 -o sugarpine_sra_merege.gtf remerge.txt   



echo -e "\nEnd time:"
date

