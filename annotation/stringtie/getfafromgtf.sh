#!/bin/bash
#SBATCH --job-name=getfafromgtf
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=80G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

module load gffread/0.12.1


#extract transcript sequences from annotation file
#gffread -w stringtie_merge_transcripts.fa -g /core/labs/Wegrzyn/ConiferGenomes/pila_2.0_june_2023/repeats/maskrepeats/combined_masked_sugarpine.fa sugarpine_all_stringtie_merged.gtf


gffread -w retrygffread_transcripts.fa -g /core/labs/Wegrzyn/ConiferGenomes/pila_2.0_june_2023/sugar_pine_genome_split_chr.fa ../gtffiles/sugarpine_all_stringtie_merged.gtf

echo -e "\nEnd time:"
date

