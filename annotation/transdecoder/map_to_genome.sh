#!/bin/bash
#SBATCH --job-name=lowresource_mapbacktogenome
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

udir="/core/labs/Wegrzyn/whitebarkpine/wbp_annotation/transdecoder/TransDecoder-TransDecoder-v5.7.0/util"
stgtf="/core/labs/Wegrzyn/ConiferGenomes/pila_2.0_june_2023/split_chromosome_hisat/top12/gtffiles/sugarpine_all_stringtie_merged.gtf"
tdout="/core/labs/Wegrzyn/ConiferGenomes/pila_2.0_june_2023/assessments/invest_stringtie/transdecoder/retrygffread_transcripts.fa.transdecoder.gff3"
fasta="/core/labs/Wegrzyn/ConiferGenomes/pila_2.0_june_2023/assessments/invest_stringtie/transdecoder/retrygffread_transcripts.fa"

$udir/gtf_to_alignment_gff3.pl $stgtf > lowresource_stringtie_to_transdecoder.gff3
$udir/cdna_alignment_orf_to_genome_orf.pl $tdout lowresource_stringtie_to_transdecoder.gff3 $fasta > lowresource_stringtie.transdecoder.genome.gff3


#grep ">" stringtie_short.fa | sed 's/>//g' > to_keep.txt
#seqkit grep -f to_keep.txt stringtie_merge_transcripts.fa.transdecoder.pep > transdecoder_remapped_to_genome.pep.fa 

echo -e "\nEnd time:"
date

