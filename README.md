# PilaGenome

#### Project information: 
```
Working directory = /core/labs/Wegrzyn/ConiferGenomes/pila_2.0_june_2023/
Location of full softmasked genome = /core/labs/Wegrzyn/ConiferGenomes/pila_2.0_june_2023/repeats/maskrepeats/combined_masked_sugarpine.fa
Location of top 12 chromosome genome = /core/labs/Wegrzyn/ConiferGenomes/pila_2.0_june_2023/repeats/maskrepeats/combined_top12_masked_sugarpine.fa
```

##Final Annotation Statistics 

| Statistics             |                                        |
| ---------------------- | -------------------------------------- |
| Total genes            | 30110                                  |
| Total transcripts      | 66000                                  |
| BUSCO                  | C:85.5%[S:42.8%,D:42.7%],F:6.1%,M:8.4% |
| EnTAP annotation rate  | 0.99                                   |
| Mono Exonic Genes      | 7404                                   |
| Multi Exonic Genes     | 22706                                  |
| Mono:Multi Ratio       | 0.33                                   |
| Max Intron Size        | 2.496 Mb                               |
| Average Exons Per mRNA | 6.3                                    |



All files related to the annotation (gff, protien, cds) can be found in the folder [annotation_files](annotation_files/)


Spreadsheet with original/full tables: https://docs.google.com/spreadsheets/d/1MMsF_jdPGBOt2oK2lPVHSL6XrheiKt7ki7ribuBpgAw/edit#gid=1336308552

## Genome Statistics 

| Full genome                  |                       |
| ---------------------------- | --------------------- |
| \# contigs                   | 40573                 |
| \# contigs (>= 0 bp)         | 40573                 |
| \# contigs (>= 1000 bp)      | 39877                 |
| \# contigs (>= 5000 bp)      | 36784                 |
| \# contigs (>= 10000 bp)     | 34272                 |
| \# contigs (>= 25000 bp)     | 26565                 |
| \# contigs (>= 50000 bp)     | 17881                 |
| Largest contig               | 2267450760            |
| Total length                 | 26485500980           |
| Total length (>= 0 bp)       | 26485500980           |
| Total length (>= 1000 bp)    | 26485031625           |
| Total length (>= 5000 bp)    | 26475129634           |
| Total length (>= 10000 bp)   | 26456366518           |
| Total length (>= 25000 bp)   | 26327063865           |
| Total length (>= 50000 bp)   | 26006627579           |
| N50                          | 2023010924            |
| N90                          | 1632947608            |
| auN                          | 1814879341            |
| L50                          | 7                     |
| L90                          | 12                    |
| GC (%)                       | 37.09                 |

***BUSCO***

	C:58.9%[S:50.2%,D:8.7%],F:23.1%,M:18.0%,n:1614	   
	952	Complete BUSCOs (C)			   
	811	Complete and single-copy BUSCOs (S)	   
	141	Complete and duplicated BUSCOs (D)	   
	373	Fragmented BUSCOs (F)			   
	289	Missing BUSCOs (M)			   
	1614	Total BUSCO groups searched	

| Top 12                       |                  |
| ---------------------------- | ---------------- |
| \# contigs                   | 12               |
| \# contigs (>= 0 bp)         | 12               |
| \# contigs (>= 1000 bp)      | 12               |
| \# contigs (>= 5000 bp)      | 12               |
| \# contigs (>= 10000 bp)     | 12               |
| \# contigs (>= 25000 bp)     | 12               |
| \# contigs (>= 50000 bp)     | 12               |
| Largest contig               | 2267450760       |
| Total length                 | 23928226539      |
| Total length (>= 0 bp)       | 23928226539      |
| Total length (>= 1000 bp)    | 23928226539      |
| Total length (>= 5000 bp)    | 23928226539      |
| Total length (>= 10000 bp)   | 23928226539      |
| Total length (>= 25000 bp)   | 23928226539      |
| Total length (>= 50000 bp)   | 23928226539      |
| N50                          | 2026340782       |
| N90                          | 1792677708       |
| auN                          | 2008823092       |
| L50                          | 6                |
| L90                          | 11               |
| GC (%)                       | 37.1             |

***BUSCO***

	C:65.5%[S:56.1%,D:9.4%],F:19.8%,M:14.7%,n:1614	   
	1058	Complete BUSCOs (C)			   
	906	Complete and single-copy BUSCOs (S)	   
	152	Complete and duplicated BUSCOs (D)	   
	319	Fragmented BUSCOs (F)			   
	237	Missing BUSCOs (M)			   
	1614	Total BUSCO groups searched		


## Genome Annotation
### Previous Annotation Statistics: Sugar pine 2.0 unscaffolded
| |  |
| -------------- | ----- | 
| Number of gene | 24455 | 
| Number of rna  | 25509 |

***BUSCO***

	C:70.3%[S:41.0%,D:29.3%],F:16.2%,M:13.5%,n:1614	   
	1135	Complete BUSCOs (C)			   
	662	Complete and single-copy BUSCOs (S)	   
	473	Complete and duplicated BUSCOs (D)	   
	261	Fragmented BUSCOs (F)			   
	218	Missing BUSCOs (M)			   
	1614	Total BUSCO groups searched		   

### Annotation of scaffolded genome using liftoff 

|			   | 	   |
| ------------ | ----- |
|Number of gene| 24436 |
|Number of rna | 25487 | 

***BUSCO***

	C:16.3%[S:10.7%,D:5.6%],F:7.1%,M:76.6%,n:1614	   
	262	Complete BUSCOs (C)			   
	172	Complete and single-copy BUSCOs (S)	   
	90	Complete and duplicated BUSCOs (D)	   
	114	Fragmented BUSCOs (F)			   
	1238	Missing BUSCOs (M)			   
	1614	Total BUSCO groups searched		   

### RNA evidence for annotating scaffolded sugar pine genome (top 12 chromosomes)
| SRA library | read number | alignment percentage (to top 12) | reads aligned | Tissue type  |
| ----------- | ----------- | -------------------- | ------------- | ------------------------ |
| SRR13823548 | 24775216    | 83.93%               | 20793839      | megagametophytes         |
| SRR13823549 | 31699295    | 85.31%               | 27042669      | megagametophytes         |
| SRR3689473  | 199547864   | 80.45%               | 160536257     | needle                   |
| SRR3696256  | 20975615    | 63.76%               | 13374052      | stem                     |
| SRR3696257  | 20381268    | 64.29%               | 13103117      | needle                   |
| SRR3712438  | 21386831    | 69.61%               | 14887373      | pollen                   |
| SRR3712439  | 23066759    | 67.66%               | 15606969      | root, stem and needles |
| SRR3712442  | 17447320    | 58.94%               | 10283450      | pollen cones             |
| SRR3723920  | 189239645   | 67.69%               | 128096316     | root                     |
| SRR3723924  | 187067815   | 76.15%               | 142452141     | root                     |
| SRR3723921  | 383267790   | 85.74%               | 328613803     | seedling                 |
| SRR3723922  | 327873882   | 87.32%               | 286299474     | embryo                   |
| SRR3723923  | 330526422   | 84.56%               | 279493142     | seedling                 |
| SRR3723925  | 350039950   | 87.72%               | 307055044     | adult tree               |
| SRR3723926  | 390666256   | 80.64%               | 315033269     | adult tree               |
| SRR3723927  | 373834496   | 84.11%               | 314432195     | seedling                 |
| SRR3712440  | 33021172    | 82.22%               | 27150008      | embryo                   |
| SRR3712441  | 42152184    | 77.07%               | 32486688      | seedling                 |

### Annotation using Stringtie2 and stringtie-merge - all libraries, post filtering for transcripts with EggNOG hit.

|			   | 	   |
| ------------ | ----- |
|Number of gene| 30,110 |
|Number of rna | 66,000 | 
|EnTAP Similarity Search Annotation rate (70/70) |  81.7% | 
| Mono-exonic | 7404 | 
| Multi-exonic | 22706 | 
| Mono:multi Ratio | 0.33 | 

***BUSCO***

	C:85.5%[S:42.8%,D:42.7%],F:6.1%,M:8.4%,n:1614	   
	1379	Complete BUSCOs (C)			   
	690	Complete and single-copy BUSCOs (S)	   
	689	Complete and duplicated BUSCOs (D)	   
	98	Fragmented BUSCOs (F)			   
	137	Missing BUSCOs (M)			   
	1614	Total BUSCO groups searched		  

## NLR Identification

### Protein domain methods

***InterProScan***


***RGAugury***

### NLR annotator 




## Identification of candidate in region of Cr1
### IGV Visualizations



***BUSCO ON LONGEST ISOFORM***

```
# BUSCO version is: 5.4.5 
# The lineage dataset is: embryophyta_odb10
# BUSCO was run in mode: proteins
```
	C:85.1%[S:78.9%,D:6.2%],F:6.1%,M:8.8%,n:1614	   
	1373	Complete BUSCOs (C)			   
	1273	Complete and single-copy BUSCOs (S)	   
	100	Complete and duplicated BUSCOs (D)	   
	98	Fragmented BUSCOs (F)			   
	143	Missing BUSCOs (M)			   
	1614	Total BUSCO groups searched		   


##### Filtering incomplete genes 

| Total genes | Total transcripts | Transcripts missing only start  | Transcripts missing only stop | Transcripts missing both | Complete genes | Transcripts from 20174 genes | 
| ----------- | ----------------- | ------------------------------- | ----------------------------- | ------------------------ | -------------- | ---------------------------- |
| 30,110      | 66,000            | 6729                            | 2968                          | 8534                     | 20174          | 50300                        | 




