#!/bin/bash
#SBATCH --job-name=run_nlr
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 24
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=80G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

ndir=/home/FCAM/abhattarai/personal_software/nlr_annotator2/

java -jar $ndir/NLR-Annotator-v2.1b.jar -i /core/labs/Wegrzyn/ConiferGenomes/pila_2.0_june_2023/top_12.fa -x $ndir/mot.txt -y $ndir/store.txt -o redo_pila2_nlrann.txt -g redo_pila2_nlrann.gff -b redg_pila2_nlrann.bed -m redo_pila2_nlrann_motif.bed -a redo_pila2_nlrann_alignment.fasta -t 24 


echo -e "\nEnd time:"
date

