#!/bin/bash
#SBATCH --job-name=run_interpro
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 18
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=80G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

module load interproscan/5.35-74.0

/isg/shared/apps/interproscan/5.35-74.0/interproscan.sh -appl Pfam,Gene3D,SUPERFAMILY,PRINTS,SMART,CDD -i sans_asterisk.pep.fa -cpu 18



echo -e "\nEnd time:"
date
