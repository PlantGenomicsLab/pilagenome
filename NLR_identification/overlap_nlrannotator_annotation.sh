#!/bin/bash
#SBATCH --job-name=overlap_and_filter
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

module load bedtools 
#awk '$3=="gene" {print}' pila2_filtered_annotation.gff > pila_gene_only.gff

#cut -d ";" -f 1 pila_gene_only.gff > pila_gene_only_short.gff
#sed -e 's/MSTRG/PIAL/g' -e 's/ID=//g' -e 's/\^scaffold.*[-+]//g' pila_gene_only_short.gff > rename_pila_gene_short.gff
bedtools intersect -f 0.8 -r -wo -a redo_pila2_nlrann.gff -b rename_pila_gene_short.gff > HELP_REDO_nlr_0.8_overlap.txt


echo -e "\nEnd time:"
date

