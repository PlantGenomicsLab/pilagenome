#!/bin/bash
#SBATCH --job-name=filt_class
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

python /core/labs/Wegrzyn/whitebarkpine/nlr_annotator/filternew.py --gff sans_asterisk.pep.fa.gff3 --out interpro_filt.gff3

module load R/4.1.2


infile=interpro_filt.gff3
outfile=interproscan_class.txt
summary=interproscan_summary.txt 
arch=interproscan_domainarch.txt

Rscript /core/labs/Wegrzyn/whitebarkpine/nlr_annotator/classifyinterproscan_wo_disease.R $infile $outfile $summary $arch 



echo -e "\nEnd time:"
date