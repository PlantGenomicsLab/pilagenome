#!/bin/bash
#SBATCH --job-name=runrga
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=40G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

module load rgaugury/2019_02_12
export PFAMDB=/home/FCAM/abhattarai/database/pfam
export PATH=$PATH:/isg/shared/apps/rgaugury/2019_02_12
export COILSDIR=/isg/shared/apps/rgaugury/2019_02_12

perl /isg/shared/apps/rgaugury/2019_02_12/RGAugury.pl -p /core/labs/Wegrzyn/ConiferGenomes/pila_2.0_june_2023/assessments/invest_stringtie/eggnog/post_eggnog_filter.pep.fa -c 12


echo -e "\nEnd time:"
date